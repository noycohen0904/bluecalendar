import {createMuiTheme} from "@material-ui/core";
import {teal, amber} from "@material-ui/core/colors";

const theme = createMuiTheme({
    palette: {
        primary: teal,
        secondary: amber
    },
    typography: {
        fontFamily: 'Tahoma'
    }
});

export default theme;