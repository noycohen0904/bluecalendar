export default interface CalendarItem {
    id: string;
    title: string;
    description: string;
};