import Task from "./Task";

export const TasksMock: Task[] = [
    {
        "id": "1",
        "title": "To praise Shperling",
        "description": "Tell your team leader that he is the best team leader you've ever had",
        "estimatedTime": "3d",
        "status": "Open",
        "priority": "High",
        "untilDate": new Date("2020-12-31T23:59:00.045Z")
    },
    {
        "id": "2",
        "title": "To finish this sub-task",
        "description": "Finish this subtask in your training program",
        "estimatedTime": "1w",
        "status": "In Progress",
        "priority": "Medium"
    },
    {
        "id": "3",
        "title": "Have a break",
        "description": "Tell your team leader that he is the best team leader you've ever had",
        "estimatedTime": "0.5h",
        "status": "Close",
        "priority": "Low",
        "review": "The break you took was 30 minutes and 2 seconds, so you have to work 2 more seconds at the end of the day",
        "timeSpent": "30m 2s",
        "untilDate": new Date("2020-12-31T23:59:00.045Z")
    },
];