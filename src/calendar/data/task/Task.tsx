import CalendarItem from "../calendarItem/CalendarItem";

export default interface Task extends CalendarItem {
    estimatedTime: string;
    status: string;
    priority: string;
    review?: string;
    timeSpent?: string;
    untilDate?: Date;
}