import Event from "./Event";
import {blue, red} from "@material-ui/core/colors";

export const EventsMock: Event[] = [
    {
        "id": "1",
        "title": "Launch break",
        "description": "Eating launch at the dinning room with my best friends :)",
        "beginningTime": new Date("1997-04-28T12:30:00.045Z"),
        "endingTime": new Date("1997-04-28T13:30:00.045Z"),
        "color": blue,
        "invitedGuests": [],
        "notificationTime": new Date("1997-04-28T11:30:00.045Z")
    },
    {
        "id": "2",
        "title": "Love your team leader",
        "description": "Say to your team leader how much he is important to you and how much you love him <3",
        "beginningTime": new Date("2020-01-01T09:00:00.045Z"),
        "endingTime": new Date("2020-01-01T09:30:00.045Z"),
        "color": red,
        "invitedGuests": ["Team Leader"],
        "notificationTime": new Date("2020-01-01T08:45:00.045Z")
    },
    {
        "id": "3",
        "title": "Go Home",
        "description": "Take your car and just drive home to your bed :)",
        "beginningTime": new Date("2020-01-01T09:00:00.045Z"),
        "endingTime": new Date("2020-01-01T09:30:00.045Z"),
        "color": red,
        "invitedGuests": ["Team Leader"],
        "notificationTime": new Date("2020-01-01T08:45:00.045Z")
    }
];