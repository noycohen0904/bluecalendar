import CalendarItem from "../calendarItem/CalendarItem";
import {Color} from "@material-ui/core";

export default interface Event extends CalendarItem {
    beginningTime: Date;
    endingTime: Date;
    color: Color;
    invitedGuests?: String[];
    location?: String;
    notificationTime?: Date;
};