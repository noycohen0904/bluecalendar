import {createStore, compose, combineReducers} from 'redux';
import {TableViewReducer} from "./tableView/TableViewReducer";
import {TasksReducer} from "./tasks/TasksReducer";
import {EventReducer} from "./events/EventReducer";

declare global {
    interface Window {
        __REDUX_DEVTOOLS_EXTENSION_COMPOSE__?: typeof compose;
    }
}

const rootReducer = combineReducers({
    tasks: TasksReducer,
    events: EventReducer,
    tableView: TableViewReducer
});

export type RootState = ReturnType<typeof rootReducer>;

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
export const store = createStore(rootReducer, composeEnhancers());
