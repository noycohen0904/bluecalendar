import {TableViewActionTypes, TASKS, TASKS_AND_EVENTS, EVENTS} from "./TableViewTypes";

export function tasksAndEvents(): TableViewActionTypes {
    return {
        type: TASKS_AND_EVENTS
    }
}

export function tasks(): TableViewActionTypes {
    return {
        type: TASKS
    }
}

export function events(): TableViewActionTypes {
    return {
        type: EVENTS
    }
}