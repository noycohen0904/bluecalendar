export interface TableViewState {
    tableView : string
}

export const TASKS_AND_EVENTS = "Tasks and Events";
export const TASKS = "Tasks";
export const EVENTS = "Events";

interface TasksAndEventsAction {
    type: typeof TASKS_AND_EVENTS
}

interface TasksAction {
    type: typeof TASKS
}

interface EventsAction {
    type: typeof EVENTS
}

export type TableViewActionTypes = TasksAndEventsAction | TasksAction | EventsAction;
