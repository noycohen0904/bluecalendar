import {
    TableViewActionTypes,
    TableViewState,
    TASKS,
    EVENTS,
    TASKS_AND_EVENTS
} from "./TableViewTypes";

const tableViewInitialState: TableViewState = {
    tableView: TASKS_AND_EVENTS
};

export function TableViewReducer(state: TableViewState = tableViewInitialState, action: TableViewActionTypes): TableViewState {
    switch (action.type) {
        case TASKS_AND_EVENTS:
            return {
                tableView: TASKS_AND_EVENTS
            };
        case TASKS:
            return {
                tableView: TASKS
            };
        case EVENTS:
            return {
                tableView: EVENTS
            };
        default:
            return state;
    }
}