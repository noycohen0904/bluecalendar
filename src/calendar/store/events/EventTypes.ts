import Event from "../../data/event/Event";

export const EVENT_LIST = "Event List";
export const ADD_EVENT = "Add Event";
export const EDIT_EVENT = "Edit Event";
export const DELETE_EVENT = "Delete Event";

export interface EventListState {
    eventList: Event[],
    counter: number
}

export interface EventListAction {
    type: typeof EVENT_LIST
}

export interface AddEventAction {
    type: typeof ADD_EVENT,
    payload: Event
}

export interface EditEventAction {
    type: typeof EDIT_EVENT,
    payload: Event
}

export interface DeleteEventAction {
    type: typeof DELETE_EVENT
    payload: {
        eventId: string
    }
}

export type EventActionTypes = EventListAction | AddEventAction | EditEventAction | DeleteEventAction;

