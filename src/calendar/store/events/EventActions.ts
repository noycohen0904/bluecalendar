import {
    ADD_EVENT,
    AddEventAction,
    DELETE_EVENT,
    DeleteEventAction,
    EDIT_EVENT,
    EditEventAction,
    EVENT_LIST,
    EventListAction
} from "./EventTypes";
import Event from "../../data/event/Event";

export function eventList(): EventListAction {
    return {
        type: EVENT_LIST
    }
}

export function addEvent(event: Event): AddEventAction {
    return {
        type: ADD_EVENT,
        payload: event
    }
}

export function editEvent(event: Event): EditEventAction {
    return {
        type: EDIT_EVENT,
        payload: event
    }
}

export function deleteEvent(eventId: string): DeleteEventAction {
    return {
        type: DELETE_EVENT,
        payload: {
            eventId: eventId
        }
    }
}