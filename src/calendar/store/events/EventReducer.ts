import {ADD_EVENT, DELETE_EVENT, EDIT_EVENT, EVENT_LIST, EventActionTypes, EventListState} from "./EventTypes";
import {EventsMock} from "../../data/event/EventsMock";

const eventInitialState: EventListState = {
    eventList: EventsMock,
    counter: EventsMock.length
};

export function EventReducer(state: EventListState = eventInitialState, action: EventActionTypes): EventListState {
    switch (action.type) {
        case EVENT_LIST:
            return {
                eventList: state.eventList,
                counter: state.counter
            };
        case ADD_EVENT:
            return {
                eventList: [...state.eventList, action.payload],
                counter: state.counter++
            };
        case EDIT_EVENT:
            return {
                eventList: state.eventList.map(event => event.id === action.payload.id ? action.payload : event),
                counter: state.counter
            };
        case DELETE_EVENT:
            return {
                eventList: state.eventList.filter(event => event.id !== action.payload.eventId),
                counter: state.counter--
            };
        default:
            return state;
    }
}