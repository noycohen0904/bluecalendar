import Task from "../../data/task/Task";

export const TASK_LIST = "Task List";
export const ADD_TASK = "Add Task";
export const EDIT_TASK = "Edit Task";
export const DELETE_TASK = "Delete Task";

export interface TaskListState {
    taskList: Task[],
    counter: number
}

interface TaskListAction {
    type: typeof TASK_LIST
}

interface AddTaskAction {
    type: typeof ADD_TASK,
    payload: Task
}

interface EditTaskAction {
    type: typeof EDIT_TASK,
    payload: Task
}

interface DeleteTaskAction {
    type: typeof DELETE_TASK,
    payload: {
        taskId: string
    }
}

export type TaskActionTypes = TaskListAction | AddTaskAction | EditTaskAction | DeleteTaskAction;