import {ADD_TASK, DELETE_TASK, EDIT_TASK, TaskActionTypes, TASK_LIST} from "./TasksTypes";
import Task from "../../data/task/Task";

export function tasksList(): TaskActionTypes {
    return {
        type: TASK_LIST
    }
}

export function addTask(task: Task): TaskActionTypes {
    return {
        type: ADD_TASK,
        payload: task
    }
}

export function editTask(task: Task): TaskActionTypes {
    return {
        type: EDIT_TASK,
        payload: task
    }
}

export function deleteTask(taskId: string): TaskActionTypes {
    return {
        type: DELETE_TASK,
        payload: {
            taskId
        }
    }
}