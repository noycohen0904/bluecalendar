import {ADD_TASK, DELETE_TASK, EDIT_TASK, TASK_LIST, TaskActionTypes, TaskListState} from "./TasksTypes";
import {TasksMock} from "../../data/task/TasksMock";

const tasksListInitialState: TaskListState = {
    taskList: TasksMock,
    counter: TasksMock.length
};

export function TasksReducer(state: TaskListState = tasksListInitialState, action: TaskActionTypes): TaskListState {
    switch (action.type) {
        case TASK_LIST:
            return {
                taskList: state.taskList,
                counter: state.counter
            };
        case ADD_TASK:
            return {
                taskList: [...state.taskList, action.payload],
                counter: state.counter++
            };
        case EDIT_TASK:
            return {
                taskList: state.taskList.map(task => task.id === action.payload.id ? action.payload : task),
                counter: state.counter
            };
        case DELETE_TASK:
            return {
                taskList: state.taskList.filter(task => task.id !== action.payload.taskId),
                counter: state.counter--
            };
        default:
            return state;
    }
}

