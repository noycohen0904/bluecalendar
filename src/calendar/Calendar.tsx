import React from 'react';
import {Provider} from "react-redux";
import {Grid, MuiThemeProvider} from "@material-ui/core";
import View from "./components/view/View";
import Navbar from "./components/navbar/Navbar";
import { store } from "./store/Store";
import theme from "./theme/Theme";

function Calendar() {
    return (
        <Provider store={store}>
            <MuiThemeProvider theme={theme}>
                <div>
                    <Grid
                        container
                        spacing = {3}
                    >
                        <View/>
                        <Navbar/>
                    </Grid>
                </div>
            </MuiThemeProvider>
        </Provider>
    );
}

export default Calendar;