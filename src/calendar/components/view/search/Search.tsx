import React from 'react';
import {createStyles, Grid, TextField} from "@material-ui/core";
import {makeStyles} from "@material-ui/core/styles";

const useStyles = makeStyles(() =>
    createStyles({
        textField: {
            width: '400px'
        }
    }),
);

function Search() {
    const {textField} = useStyles();

    return (
        <Grid item>
            <TextField id="outlined-basic" label="Enter text here to search" variant="outlined" className={textField}/>
        </Grid>
    );
}

export default Search;