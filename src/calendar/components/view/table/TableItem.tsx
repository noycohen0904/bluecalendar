import React from 'react';
import {TableCell} from "@material-ui/core";

export interface TableItemProps {
    id: string;
    data: string;
}

function TableItem(props: TableItemProps) {
    return (
        <TableCell key={props.id} align="center">
            {props.data}
        </TableCell>
    );
}

export default TableItem;