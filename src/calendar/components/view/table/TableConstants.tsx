import {TableHeadTitleProps} from "./CostumeTableHead";

export const commonTitles: TableHeadTitleProps[] = [
    {id: "type", label: "Type"},
    {id: "priority", label: "Priority"},
    {id: "title", label: "Title"},
    {id: "other", label: "Other"},
    {id: "actions", label: "Actions"}
];

export const onlyTasksTitles: TableHeadTitleProps[] = [
    {id: "type", label: "Type"},
    {id: "priority", label: "Priority"},
    {id: "title", label: "Title"},
    {id: "status", label: "Status"},
    {id: "estimate_time", label: "Estimate Time"},
    {id: "other", label: "Other"},
    {id: "actions", label: "Actions"}
];

export const onlyEventsTitles: TableHeadTitleProps[] = [
    {id: "color", label: "Color"},
    {id: "title", label: "Title"},
    {id: "from", label: "From"},
    {id: "until", label: "Until"},
    {id: "location", label: "Location"},
    {id: "actions", label: "Actions"}
];