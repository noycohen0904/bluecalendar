import React from 'react';
import {TableContainer, Table, Grid, Paper} from "@material-ui/core";
import CostumeTableHead from "./CostumeTableHead";
import {useSelector} from "react-redux";
import {TASKS, TASKS_AND_EVENTS} from "../../../store/tableView/TableViewTypes";
import {commonTitles, onlyEventsTitles, onlyTasksTitles} from "./TableConstants";
import {makeStyles} from "@material-ui/styles";
import {RootState} from "../../../store/Store";
import CostumeTableBody from "./CostumeTableBody";
import CalendarItem from "../../../data/calendarItem/CalendarItem";

const useStyles = makeStyles({
    table: {
        width: '80%',
    },
});

function CostumeTable() {
    const {table} = useStyles();

    const tableView = useSelector((state: RootState) => state.tableView.tableView);
    const tasks = useSelector((state: RootState) => state.tasks.taskList);
    const events = useSelector((state: RootState) => state.events.eventList);
    const allEventsAndTasks: CalendarItem[] = [...tasks, ...events];

    return (
        <Grid item className={table}>
            <TableContainer component={Paper}>
                <Table aria-label="simple table">
                    {
                        (tableView === TASKS_AND_EVENTS) ?
                            <>
                                <CostumeTableHead titles={commonTitles}/>
                                <CostumeTableBody list={allEventsAndTasks} titles={commonTitles}/>
                            </>
                            :
                            (tableView === TASKS) ?
                                <>
                                    <CostumeTableHead titles={onlyTasksTitles}/>
                                    <CostumeTableBody list={tasks} titles={onlyTasksTitles}/>
                                </>
                                :
                                <>
                                    <CostumeTableHead titles={onlyEventsTitles}/>
                                    <CostumeTableBody list={events} titles={onlyEventsTitles}/>
                                </>
                    }
                </Table>
            </TableContainer>
        </Grid>
    )
}

export default CostumeTable;