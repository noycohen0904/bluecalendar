import React from 'react';
import {TableHead, TableRow} from "@material-ui/core";
import TableItem from "./TableItem";

export interface TableHeadTitleProps {
    id: string;
    label: string;
}

export interface TableHeadProps {
    titles: TableHeadTitleProps[]
}

function CostumeTableHead(props: TableHeadProps) {
    return (
        <TableHead>
            <TableRow>
                {props.titles.map((title) => (
                    <TableItem id={title.id} data={title.label}/>
                ))}
            </TableRow>
        </TableHead>
    )
}

export default CostumeTableHead;