import React from 'react';
import {Grid} from "@material-ui/core";
import CostumeButton from "../costumeButton/CostumeButton";

function Filters() {
    return(
        <Grid item>
            <Grid container spacing={4}>
                <CostumeButton title="Only tasks"/>
                <CostumeButton title="Only events"/>
                <CostumeButton title="Uncompleted tasks"/>
                <CostumeButton title="High priority tasks"/>
            </Grid>
        </Grid>
    );
}

export default Filters;