import React from 'react';
import {Grid} from "@material-ui/core";
import Search from "./search/Search";
import Filters from "./filters/Filters";
import CostumeTable from "./table/CostumeTable";
import CostumeButton from "./costumeButton/CostumeButton";

function View() {
    return (
            <Grid
                item
                container
                direction="column"
                justify="center"
                alignItems="center"
                xs={9}
                spacing={3}>
                <Search/>
                <Filters/>
                <CostumeTable/>
                <CostumeButton title={"Add"}/>
            </Grid>
    );
}

export default View;