import React from 'react';
import {Button, createStyles, Grid} from "@material-ui/core";
import {makeStyles} from "@material-ui/core/styles";

interface CostumeButtonProps {
    title: string;
}

const useStyles = makeStyles(() =>
    createStyles({
        button: {
            textTransform: 'none',
            width: "150px"
        }
    }),
);

function CostumeButton(props : CostumeButtonProps) {
    const {button} = useStyles();

    return(
        <Grid item>
            <Button variant="outlined" color="secondary" className={button}>{props.title}</Button>
        </Grid>
    );
}

export default CostumeButton;