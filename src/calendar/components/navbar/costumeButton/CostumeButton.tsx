import React from 'react';
import {Button, createStyles, Grid} from '@material-ui/core';
import {makeStyles} from "@material-ui/core/styles";
import {store} from "../../../store/Store";

interface ButtonProps {
    title: string;
    action: () => typeof store.dispatch;
}

const useStyles = makeStyles(() =>
    createStyles({
        button: {
            width: '230px'
        }
    }),
);

function CostumeButton(props: ButtonProps) {
    const {button} = useStyles();

    return (
        <Grid item>
            <Button variant="contained" color="primary" className={button} onClick={props.action}>
                {props.title}
            </Button>
        </Grid>
    );
}

export default CostumeButton;