import {createStyles, Grid} from "@material-ui/core";
import React from "react";
import logo from './logo.jpg';
import {makeStyles} from "@material-ui/core/styles";

const useStyles = makeStyles(() =>
    createStyles({
        image: {
            width: '150px'
        }
    }),
);

function Logo() {
    const {image} = useStyles();

    return (
        <Grid item>
            <img className={image} src={logo} alt="torch"/>
        </Grid>
    );
}

export default Logo;