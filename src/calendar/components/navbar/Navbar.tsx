import React from 'react';
import {Grid} from "@material-ui/core";
import CostumeClock from "./costumeClock/CostumeClock";
import Logo from './logo/Logo';
import Typography from '@material-ui/core/Typography';
import CostumeButton from "./costumeButton/CostumeButton";
import {events, tasks, tasksAndEvents} from "../../store/tableView/TableViewActions";
import {connect} from "react-redux";
import {store} from "../../store/Store";

const mapDispatchToProps = (dispatch: any) => {
    return {
        tasksAndEvents: () => dispatch(tasksAndEvents()),
        tasks: () => dispatch(tasks()),
        events: () => dispatch(events())
    }
};

interface navbarProps {
    tasksAndEvents: () => typeof store.dispatch,
    tasks: () => typeof store.dispatch,
    events: () => typeof store.dispatch
}

function Navbar(props: navbarProps) {
    return (
        <Grid item xs={3}>
            <Grid
                container
                direction="column"
                justify="space-between"
                alignItems="center"
                spacing={3}
            >
                <CostumeClock />
                <Typography variant="button" display="block" gutterBottom>Blue Calendar</Typography>
                <Logo />
                <CostumeButton title={"Today tasks and events"} action={props.tasksAndEvents}/>
                <CostumeButton title={"Tasks list"} action={props.tasks}/>
                <CostumeButton title={"Events list"} action={props.events}/>
            </Grid>
        </Grid>
    );
}

export default connect(null, mapDispatchToProps)(Navbar);