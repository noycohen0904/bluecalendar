import React from 'react';
import {Grid} from "@material-ui/core";
import Clock from 'react-live-clock';

function CostumeClock() {
    return (
        <Grid item>
            <Clock format={'HH:mm:ss DD-MM-YYYY'} ticking={true} timezone={'Asia/Jerusalem'} />
        </Grid>
    );
}

export default CostumeClock;